#!/bin/sh
#启动用户
RUNNING_USER=icehand
JAVA_HOME="/home/java/jdk1.8.0_101"
JAVA_OPTS="jpushjob.jar"
APP_HOME=/home/fashion/provider
LOGS=/home/fashion/provider/jpushjob/jpushjob.log
APP_MAINCLASS=jpushjob
JAVA_CMD=" $JAVA_HOME/bin/java -jar  $JAVA_OPTS "
#初始化psid变量（全局）
psid=0
 
checkpid() {
   javaps=`$JAVA_HOME/bin/jps -l | grep $JAVA_OPTS`
 
   if [ -n "$javaps" ]; then
      psid=`echo $javaps | awk '{print $1}'`
   else
      psid=0
   fi
}
 
###################################
#(函数)启动程序
###################################
start() {
   checkpid
 
   if [ $psid -ne 0 ]; then
      echo "================================"
      echo "warn: $APP_MAINCLASS already started! (pid=$psid)"
      echo "================================"
     else
      echo -n "Starting $APP_MAINCLASS ..."
     # JAVA_CMD=" $JAVA_HOME/bin/java $JAVA_OPTS >${LOGS} 2>&1 &"
      cd  $APP_HOME/${APP_MAINCLASS}
     # su  $RUNNING_USER -c "$JAVA_CMD "
     $JAVA_CMD >${LOGS} 2>&1 &
      checkpid
         if [ $psid -ne 0 ]; then
            echo "(pid=$psid) [OK]"
                    else
             echo "[Failed]"
      fi
   fi
}
 
###################################
#(函数)停止程序
###################################
stop() {
   checkpid
 
   if [ $psid -ne 0 ]; then
      echo -n "Stopping $APP_MAINCLASS ...(pid=$psid) "
      kill -9 $psid
      if [ $? -eq 0 ]; then
         echo "[OK]" 
       else
         echo "[Failed]"
      fi
 
      checkpid
      if [ $psid -ne 0 ]; then
         stop
      fi
   else
      echo "================================"
      echo "warn: $APP_MAINCLASS is not running"
      echo "================================"
   fi
}
 
###################################
#(函数)检查程序运行状态
#
#说明：
#1. 首先调用checkpid函数，刷新$psid全局变量
#2. 如果程序已经启动（$psid不等于0），则提示正在运行并表示出pid
#3. 否则，提示程序未运行
###################################
status() {
   checkpid
 
   if [ $psid -ne 0 ];  then
      echo "$APP_MAINCLASS is running! (pid=$psid)"
   else
      echo "$APP_MAINCLASS is not running"
   fi
}
 
###################################
#(函数)打印系统环境参数
###################################
info() {
   echo "System Information:"
   echo "****************************"
   echo `head -n 1 /etc/issue`
   echo `uname -a`
   echo
   echo "JAVA_HOME=$JAVA_HOME"
   echo `$JAVA_HOME/bin/java -version`
   echo
   echo "APP_HOME=$APP_HOME"
   echo "****************************"
}
 
###################################
#读取脚本的第一个参数($1)，进行判断
#参数取值范围：{start|stop|restart|status|info}
#如参数不在指定范围之内，则打印帮助信息
###################################
case "$1" in
   'start')
      start
      ;;
   'stop')
     stop
     ;;
   'restart')
     stop
     start
     ;;
   'status')
     status
     ;;
   'info')
     info
     ;;
  *)
     echo "Usage: $0 {start|stop|restart|status|info}"
     exit 1
;;
esac

