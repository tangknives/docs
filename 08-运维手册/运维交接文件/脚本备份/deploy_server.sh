#!/bin/sh
#启动用户
RUNNING_USER=icehand
JAVA_HOME="/home/java/jdk1.8.0_101"
JAVA_OPTS=""
APP_HOME=/home/fashion/provider
#LOGS=/home/web/logs/webffo.log
APP_MAINCLASS=""
JAVA_CMD=" $JAVA_HOME/bin/java -jar  *.jar  "
BACK_HOME=/home/fashion/provider/back_up
DEPLOY_HOME=/home/fashion/provider/deploy
date=` date +%Y%m%d `
#初始化psid变量（全局）
psid=0
back_file=0
deploy_file=0

#确认进程 
checkpid() {
   javaps=`$JAVA_HOME/bin/jps -l | grep $APP_MAINCLASS`
 
   if [ -n "$javaps" ]; then
      psid=`echo $javaps | awk '{print $1}'`
   else
      psid=0
   fi
}
#确认备份文件
check_back_file() 
{
     if [ ! -d "$BACK_HOME" ];then
      echo "================================"
      echo "warn: The  back_up  folder is  not  exist,Starting  creat..."
      echo "================================"   
       mkdir  $BACK_HOME 
	  
	  fi    
     if [[ ! -d  $BACK_HOME/${APP_MAINCLASS}_$date ]];then
        
             back_file=0		
       else  
	       back_file=1
       	   
 	  fi	 
	  
	  
}
#确认发布文件
check_deploy_file()	 
{
    if [ ! -d "$DEPLOY_HOME/" ];then
	  echo "================================"
      echo "warn: The  deploy  folder is  not  exist,Starting  creat..."
      echo "================================"   
        mkdir  $DEPLOY_HOME/deploy
      
	  fi 
       
     if [[ ! -d $DEPLOY_HOME/${APP_MAINCLASS} ]];then
          deploy_file=0	
       else  
	  deploy_file=1
       	   
 	  fi	 
	  
  
	  
}
###################################
#(函数)备份
###################################
backup() 
   {
   
if [ ! -d  "$BACK_HOME" ]; then
     mkdir  $BACK_HOME
fi

if [ ! -d  "$APP_HOME/$APP_MAINCLASS" ]; then
    echo   "warm: The $APP_HOME/$APP_MAINCLASS is not exist"
else
     if [[ ! -d  "$BACK_HOME/${APP_MAINCLASS}_$date" ]]; then  
          cp -r $APP_HOME/$APP_MAINCLASS   $BACK_HOME/${APP_MAINCLASS}_$date
          echo "Run  Backup, The backup folder is  $BACK_HOME/${APP_MAINCLASS}__$date"
     else
         echo   "Today  I have  backuped  the  ${APP_MAINCLASS},The backup folder is  $BACK_HOME/${APP_MAINCLASS}__$date"      
   fi   
 fi
   }
   

###################################
#(函数)发布
###################################
deploy_server()
{    
while true
do
        #menu
		echo   "=========================================================="
        printf "服务列表：\n "
        printf "{user|product|words|message|es|directive|executor|exit}\n "
		echo   "=========================================================="

        printf "warn：输入[exit]退出\n "
        read -p "请输入你要发布的服务：" select
		echo   "=========================================================="
        APP_MAINCLASS=$select
        case "$APP_MAINCLASS" in
   'user')
        echo  "开始发布 $APP_MAINCLASS  服务"
		echo   "=========================================================="
        deploy
      ;;
   'product')
      echo  "开始发布 $APP_MAINCLASS  服务"
	  echo   "=========================================================="
        deploy
     ;;
   'words')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
   'message')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
   'es')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
   'directive')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
    'jpushjob')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
	 'payment')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
	 'order')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
	 'mqcenter')
     echo  "开始发布 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        deploy
     ;;
	   'exit')
      break
     ;;
  *)
     echo "Usage: $0 {user|product|words|message|es|directive|executor|exit}"
	 echo "================================================================="
     continue
     ;;
     esac
 done
}

###################################
#(函数)发布服务
###################################
deploy()
 {
     check_deploy_file   
     if [ $deploy_file -ne 1 ] ;then
	    echo "================================"
        echo "warn: The  deploy_file  $APP_MAINCLASS  is not  exist "
        echo "================================" 
      else	
	    backup
            rm -rf  $APP_HOME/$APP_MAINCLASS/config
            rm -rf  $APP_HOME/$APP_MAINCLASS/lib
            rm -rf  $APP_HOME/$APP_MAINCLASS/${APP_MAINCLASS}.jar
        
            
             echo "Copy the new files"
             cp  -r  $DEPLOY_HOME/$APP_MAINCLASS/config  $APP_HOME/$APP_MAINCLASS
	     cp  -r  $DEPLOY_HOME/$APP_MAINCLASS/lib     $APP_HOME/$APP_MAINCLASS
             cp      $DEPLOY_HOME/$APP_MAINCLASS/*.jar   $APP_HOME/$APP_MAINCLASS
		
         stop_server
            
         start_server	
     
     
      if [ $psid -ne 0 ]; then
            echo "Deploy  $APP_MAINCLASS  successful (pid=$psid) [OK]"
                    else
             echo "Deploy Failed,Now running  rollback"
			  rollback
      fi
     fi		
	 	   
}
backup_server()
{
while true
do
        #menu
		echo   "=========================================================="
        printf "服务列表：\n "
        printf "{user|product|words|message|es|directive|jpushjob|payment|order|mqcenter|exit}\n "
		echo   "=========================================================="

        printf "warn：输入[exit]退出\n "
        read -p "请输入你要备份的服务：" select
		echo   "=========================================================="
        APP_MAINCLASS=$select
        case "$APP_MAINCLASS" in
   'user')
        echo  "开始备份 $APP_MAINCLASS  服务"
       echo   "=========================================================="
        backup
      ;;
   'product')
      echo  "开始备份 $APP_MAINCLASS  服务"
	  echo   "=========================================================="
        backup
     ;;
   'words')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
   'message')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
   'es')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
   'directive')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
    'jpushjob')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
	 'payment')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
	 'order')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
	 'mqcenter')
     echo  "开始备份 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        backup
     ;;
	   'exit')
      break
     ;;
  *)
     echo "Usage: $0 {user|product|words|message|es|directive|jpushjob|payment|order|mqcenter|exit}"
	 echo "================================================================="
     continue
     ;;
     esac
 done

}
rollback_server()
{
while true
do
        #menu
		echo   "=========================================================="
        printf "服务列表：\n "
        printf "{user|product|words|message|es|directive|jpush||payment|order|mqcenter|exit}\n "
		echo   "=========================================================="

        printf "warn：输入[exit]退出\n "
        read -p "请输入你要回滚的服务：" select
		echo   "=========================================================="
        APP_MAINCLASS=$select
        case "$APP_MAINCLASS" in
   'user')
        echo  "开始回滚 $APP_MAINCLASS  服务"
		echo   "=========================================================="
        rollback
      ;;
   'product')
      echo  "开始回滚 $APP_MAINCLASS  服务"
	  echo   "=========================================================="
        rollback
     ;;
   'words')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
   'message')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
   'es')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
   'directive')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
    'jpushjob')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
	 'payment')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
	 'order')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
	 'mqcenter')
     echo  "开始回滚 $APP_MAINCLASS  服务"
	 echo   "=========================================================="
        rollback
     ;;
	   'exit')
      break
     ;;
  *)
     echo "Usage: $0 {user|product|words|message|es|directive|jpushjob|payment|order|mqcenter|exit}"
	 echo "================================================================="
     continue
     ;;
     esac
 done

}


rollback()
{   
     check_back_file
     if [ $back_file -ne 1 ] ;then
	    echo "================================"
        echo "warn: The  back_file $APP_MAINCLASS is not  exist "
        echo "================================" 
    else
         echo  "rolling  back  ..."	
		 #删除原包
		 rm -rf  $APP_HOME/$APP_MAINCLASS/config
         rm -rf  $APP_HOME/$APP_MAINCLASS/lib
         rm -rf  $APP_HOME/$APP_MAINCLASS/*.jar
          #拷贝备份文件夹文件  
	    cp  -r  $BACK_HOME/${APP_MAINCLASS}_$date/config  $APP_HOME/$APP_MAINCLASS
	    cp  -r  $BACK_HOME/${APP_MAINCLASS}_$date/lib  $APP_HOME/$APP_MAINCLASS
	    cp  -r  $BACK_HOME/${APP_MAINCLASS}_$date/*.jar  $APP_HOME/$APP_MAINCLASS
            

           stop_server
          start_server	

	  
     fi	 


	 
}

###################################
#(函数)启动程序
###################################
start_server() {
   checkpid
 
   if [ $psid -ne 0 ]; then
      echo "================================"
      echo "warn: $APP_MAINCLASS already started! (pid=$psid)"
      echo "================================"
      else
      echo  "Starting $APP_MAINCLASS ........"
     # JAVA_CMD=" $JAVA_HOME/bin/java $JAVA_OPTS >${LOGS} 2>&1 &"
      cd  $APP_HOME/$APP_MAINCLASS
     # su  $RUNNING_USER -c "$JAVA_CMD "
     $JAVA_CMD  &
	 #等待5秒后检查服务存活
	  echo -n  "Checking  alive "
      sleep  1	
      echo  -e ".\c"
      sleep  1	
      echo  -e ".\c"
      sleep  1	
      echo  -e ".\c"
      sleep  1	
      echo  -e ".\c"
      sleep  1	
      echo  -e "."	  
      checkpid
         if [ $psid -ne 0 ]; then
            echo "The server is running (pid=$psid) [OK]"
                    else
             echo "The server is not running [Failed]"
      fi
   fi
}
 
###################################
#(函数)停止程序
###################################
stop_server() {
   checkpid
 
   if [ $psid -ne 0 ]; then
      echo -n "Stopping $APP_MAINCLASS ...(pid=$psid) "
      kill -9 $psid
     echo  "kill Ok"
      if [ $? -eq 0 ]; then
         echo "[OK]" 
       else
         echo "[Failed]"
      fi
 
      checkpid
      if [ $psid -ne 1 ]; then
         stop_server
      fi
   else
      echo "================================"
      echo "warn: $APP_MAINCLASS is not running"
      echo "================================"
   fi
}
 
###################################
#(函数)检查程序运行状态
#
#说明：
#1. 首先调用checkpid函数，刷新$psid全局变量
#2. 如果程序已经启动（$psid不等于0），则提示正在运行并表示出pid
#3. 否则，提示程序未运行
###################################
status() {
   checkpid
 
   if [ $psid -ne 0 ];  then
      echo "$APP_MAINCLASS is running! (pid=$psid)"
   else
      echo "$APP_MAINCLASS is not running"
   fi
}
 
###################################
#(函数)打印系统环境参数
###################################
info() {
   echo "System Information:"
   echo "****************************"
   echo `head -n 1 /etc/issue`
   echo `uname -a`
   echo
   echo "JAVA_HOME=$JAVA_HOME"
   echo `$JAVA_HOME/bin/java -version`
   echo
   echo "APP_HOME=$APP_HOME"
   echo "****************************"
}
 
###################################
#读取脚本的第一个参数($1)，进行判断
#参数取值范围：{deploy|rollback|backup}
#如参数不在指定范围之内，则打印帮助信息
###################################
case "$1" in
   'deploy')
     deploy_server
     ;;
   'rollback')
     rollback_server
     ;;	 
   'backup')
     backup_server
     ;;
  *)
     echo "Usage: $0 {deploy|rollback|backup}"
     exit 1
;;
esac




