#!/bin/sh
date=` date +%Y%m%d `      #获取当前日期
DAYS=7                     #DAYS=7代表删除7天前的备份，即只保留最近7天的备份
BK_DR=/home/mysql_bak  #备份文件存放路径
DB_DR=/var/lib/mysql   #数据库路径

LINUX_USER=root           #系统用户名
/usr/local/mysql/bin/mysqldump -uroot -p123456   --all-databases > $BK_DR/all_databases_147.sql
tar zcvf $BK_DR/mysql_data_147_$date.tar.gz  $BK_DR/all_databases_147.sql  #备份数据

chown -R $LINUX_USER:$LINUX_USER $BK_DR  #更改备份数据库文件的所有者

find $BK_DR -name "mysql_data*" -type f -mtime +$DAYS -exec rm {} \; 

deldate=` date -d -7day +%Y_%m_%d ` 

ftp -n <<!
open  172.16.8.151 21

user ftp ftp@123456

binary

cd mysql_bak

lcd /home/mysql_bak/

prompt

mput mysql_data_147_$date.tar.gz   mysql_data_147_$date.tar.gz

mdelete mysql_data_147_$deldate.tar.gz mysql_data_147_$deldate.tar.gz

close #关闭
bye ! #退出
