#!/bin/sh 
# chkconfig:   2345 90 10
# description:  Redis is a persistent key-value database

#redis主目录
REDIS_HOME=/home/redis-cluster
#redis 主机
HOST=172.16.8.147

#nodes文件目录
REDIS_NODIS=/home/redis-cluster/nodes
REDIS_PID=/home/redis-cluster/pid
REDIS_DATA=/home/redis-cluster/data
#每台机器上生成的redis实例数量  
NODES_PER_HOST=10 

#每台机器的redis起始端口号，10台机器就是6379-6388  
START_PORT=6379  
  
#每组redis的slave机器的数量  
REPLICAS=1 

redis_local_init()
   {
    
   
   rm  -f    $REDIS_NODIS/* 
   rm -f     $REDIS_DATA/*
 
   }
   
redis_local_start()
   {
   if [ "$(ls $REDIS_PID)"   ]; then
    echo "Redis-Cluster  is already running in $HOST"
     else   
       for(( i=0; i<$NODES_PER_HOST; i++))  
            do  
             PORT=$((START_PORT+i))  
             $REDIS_HOME/redis-server $REDIS_HOME/conf/$PORT.conf 
             echo "start redis at port $PORT"
       done 
       
           $REDIS_HOME/./redis-trib.rb create --replicas 1 $HOST:6379 $HOST:6380 $HOST:6381  $HOST:6382 $HOST:6383 $HOST:6384 $HOST:6385 $HOST:6386 $HOST:6387 $HOST:6388
      # else 
       #      echo  "redis is already  running in $HOST"
           
    fi
  
   }
   
redis_local_stop() {  
    for(( i=0; i<$NODES_PER_HOST; i++))  
        do  
                PORT=$((START_PORT+i))  
        echo "stop redis at port $PORT"  
                $REDIS_HOME/src/redis-cli -h $HOST -p $PORT shutdown  
        done 
     
}  
redis_local_restart()
{
       
	   redis_local_stop
	   redis_local_init
	   redis_local_start
}

case "$1" in  
    start)  
#启动  
        redis_local_init
        redis_local_start  
    ;;  
    stop)  
#停止
        redis_local_stop  
    ;;  
    restart)  
#重启
        redis_local_restart  
    ;;  
    init)  
#初始化  
        redis_local_init  
    ;;  
	*)  
        echo $"Usage: $0 {start|stop|init|restart}"  
        exit 1  
esac  

