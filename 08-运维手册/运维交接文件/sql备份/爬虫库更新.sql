--  "012"  net-a-port      "013"   ASOS
--  更新  爬虫sku
UPDATE fashion_product.t_spider_product_sku a
LEFT JOIN spider_sku.t_spider_product_sku b ON a.spider_product_sku_id = b.spider_product_sku_id
SET a.stock_status = b.stock_status
WHERE
	b.stock_status = 0;

--  更新 爬虫商品库欧洲原价
UPDATE fashion_product.t_spider_product_sku a
LEFT JOIN spider_sku.t_spider_product_sku b ON a.spider_product_sku_id = b.spider_product_sku_id
SET a.price = b.price
WHERE
	a.price <> b.price;

-- 更新爬虫商品库折扣价
UPDATE fashion_product.t_spider_product_sku a
LEFT JOIN spider_sku.t_spider_product_sku b ON a.spider_product_sku_id = b.spider_product_sku_id
SET a.discount_price = b.discount_price
WHERE
	a.discount_price <> b.discount_price;

-- 存档
UPDATE t_spider_product a
SET a.`status` = 5
WHERE
	NOT EXISTS (
		SELECT
			*
		FROM
			t_spider_product_sku
		WHERE
			spider_product_id = a.spider_product_id
		AND stock_status = 1
	);

-- 更新product——sku
UPDATE t_product_sku a
LEFT JOIN t_product b ON a.product_id = b.product_id
LEFT JOIN spider_sku.t_spider_product_sku c ON b.spider_product_id = c.spider_product_id
AND a.size = c.size
SET a.sku_status = "OFF",
 b.modify_time = UNIX_TIMESTAMP(NOW()) ,
 
WHERE
	b.source IN (
		"013",
		"012",
		"001",
		"002",
		"003",
		"004",
		"005",
		"006",
		"007",
		"008",
		"009",
		"010",
		"011",
		"015",
		"016",
		"017",
		"017",
		"018",
		"019",
		"020",
		"021"
	)
AND c.stock_status = 0;

UPDATE t_product a
SET a.product_status = 0,
 a.modify_time = UNIX_TIMESTAMP(NOW()) ,
 a.editor_name=panwang
WHERE
	a.source IN (
		"013",
		"012",
		"001",
		"002",
		"003",
		"004",
		"005",
		"006",
		"007",
		"008",
		"009",
		"010",
		"011",
		"015",
		"016",
		"017",
		"017",
		"018",
		"019",
		"020",
		"021"
	)
AND product_status = 1
AND NOT EXISTS (
	SELECT
		product_sku_id
	FROM
		t_product_sku
	WHERE
		product_id = a.product_id
	AND sku_status = 'ON'
);

