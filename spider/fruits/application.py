# -*- coding: UTF-8 -*-
from flask import Flask
from flask import render_template
from flask_bootstrap import Bootstrap
import base64
import json
import sys

reload(sys)  # Python2.5 初始化后会删除 sys.setdefaultencoding 这个方法，我们需要重新载入
sys.setdefaultencoding('utf-8')

app = Flask(__name__)
Bootstrap(app)

fruits = [
    {
        "title": "砂糖橘",
        "unit": "元/斤",
        "price": "3.98",
        "image_url": "/static/img/orange.jpg"
    },
    {
        "title": "贡梨",
        "unit": "元/斤",
        "price": "2.98",
        "image_url": "/static/img/pear.jpg"
    },
    {
        "title": "冰糖心野生苹果",
        "unit": "元/斤",
        "price": "5.98",
        "image_url": "/static/img/apple.jpg"
    },
    {
        "title": "冰糖柑",
        "unit": "元/斤",
        "price": "2.98",
        "image_url": "/static/img/bingtanggan.jpg"
    },
    {
        "title": "香梨",
        "unit": "元/3斤",
        "price": "10",
        "image_url": "/static/img/pear_xiang.jpg"
    },
    {
        "title": "大板栗",
        "unit": "元/斤",
        "price": "6.8",
        "image_url": "/static/img/banli.jpg"
    },
    {
        "title": "肉桂",
        "unit": "元/斤",
        "price": "6.58",
        "image_url": "/static/img/rougui.jpg"
    },
    {
        "title": "香蕉",
        "unit": "元/斤",
        "price": "2.48",
        "image_url": "/static/img/banana.jpg"
    },
    {
        "title": "蛇果",
        "unit": "元/斤",
        "price": "4.98",
        "image_url": "/static/img/sheguo.jpg"
    },
    {
        "title": "火龙果",
        "unit": "元/3个",
        "price": "10",
        "image_url": "/static/img/huolongguo.jpg"
    },
    {
        "title": "脐橙",
        "unit": "元/斤",
        "price": "4.88",
        "image_url": "/static/img/jicheng.jpg"
    },
    {
        "title": "葡萄柚",
        "unit": "元/3个",
        "price": "10",
        "image_url": "/static/img/putaoyo.jpg"
    },
    {
        "title": "榴莲",
        "unit": "元/斤",
        "price": "19.8",
        "image_url": "/static/img/liulian.jpg"
    },
    {
        "title": "吸卡",
        "unit": "元/排",
        "price": "8.5",
        "image_url": "/static/img/xika.jpg"
    },
    {
        "title": "勺卡",
        "unit": "元/排",
        "price": "10.5",
        "image_url": "/static/img/shaoka.jpg"
    },
    {
        "title": "小光明牛奶",
        "unit": "元/盒",
        "price": "5",
        "image_url": "/static/img/guangming_milk.jpg"
    },
    {
        "title": "榴莲饼",
        "unit": "元/袋",
        "price": "14.8",
        "image_url": "/static/img/liulianbing.jpg"
    },
    {
        "title": "纳宝帝威化饼",
        "unit": "元/盒",
        "price": "9.9",
        "image_url": "/static/img/weihua.jpg"
    },
    {
        "title": "安慕希",
        "unit": "元/件",
        "price": "55",
        "image_url": "/static/img/anmuxi.jpg"
    },
    {
        "title": "金典纯牛奶",
        "unit": "元/提",
        "price": "55",
        "image_url": "/static/img/jindian.jpg"
    },
    {
        "title": "伊利纯牛奶",
        "unit": "元/提",
        "price": "45",
        "image_url": "/static/img/yili.jpg"
    }
]


@app.route('/')
@app.route('/fruits')
def index():
    return render_template("fruits.html", fruits=fruits, base64=base64, json=json)


if __name__ == '__main__':
    app.run()
