function showBigImage(loop_index, images_store_id) {
    var image_urls_string = $(images_store_id).data("image-urls")
    var image_urls = $.parseJSON(image_urls_string)
    $("#myCarousel .carousel-indicators").html("")
    $("#myCarousel .carousel-inner").html("")
    $("#myImageGrid9").html("")
    for (var i in image_urls) {
        var indicator = $('<li data-target="#myCarousel"></li>')
        indicator.attr("data-slide-to", i)
        if (i === loop_index) {
            indicator.addClass("active")
        }
        $("#myCarousel .carousel-indicators").append(indicator)

        var carousel = $('<div class="item">')
        if (i === loop_index) {
            carousel.addClass("active")
        }

        carousel.append($('<img>').attr("src", "/static/" + image_urls[i]))
        $("#myCarousel .carousel-inner").append(carousel)

        var image_grid = $('<div class="col-md-4">')
        image_grid.append($('<img>').addClass(".img-thumbnail").attr("src", "/static/" + image_urls[i]))
        $("#myImageGrid9").append(image_grid)
    }


    $("#image-modal").modal()
}

function showBigImageGrid9() {
    $("#myCarousel").hide()
    $("#myImageGrid9").show()
}

function showBigImageCarousel() {
    $("#myCarousel").show()
    $("#myImageGrid9").hide()
}

function nextPage() {
    window.location.href = "/movies/{{page_index + 1}}"
}

function prevPage() {
    window.location.href = "/movies/{{page_index - 1}}"
}

function openPage(page_index) {
    window.location.href = "/movies/" + page_index
}

document.onkeydown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (e && e.keyCode == 27) { // 按 Esc
        //要做的事情
    }
    if (e && e.keyCode == 113) { // 按 F2
        //要做的事情
    }
    if (e && e.keyCode == 13 && e.ctrlKey) { // enter 键 & ctrl
        //要做的事情
        var page_index = prompt('输入页码', '{{next_page}}');
        if (page_index) {
            openPage(page_index)
        }
    }

    if (e && e.keyCode == 37) { // <-
        //要做的事情
        prevPage()
    }
    if (e && e.keyCode == 39) { // ->
        //要做的事情
        nextPage()
    }
    console.log(e.keyCode)
}