# -*- coding: UTF-8 -*-
import pymysql


class SchemaRepository():
    def __init__(self):
        self.connection = pymysql.connect(host='172.16.8.147',
                                          port=3306,
                                          user='dba',
                                          passwd='123456',
                                          db='information_schema',
                                          cursorclass=pymysql.cursors.DictCursor,
                                          charset='utf8mb4')

    def get_table_columns(self, table_name, table_schema):
        sql = """
        SELECT * FROM information_schema.columns WHERE table_name=%s AND table_schema=%s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (table_name, table_schema))
            return cursor.fetchall()


class MacroRepository():
    def __init__(self):
        self.connection = pymysql.connect(host='172.16.8.147',
                                          port=3306,
                                          user='dba',
                                          passwd='123456',
                                          db='directive',
                                          cursorclass=pymysql.cursors.DictCursor,
                                          charset='utf8mb4')

    def get_macro(self, name):
        sql = "SELECT * FROM t_directive_macro WHERE `name`=%s"
        with self.connection.cursor() as cursor:
            cursor.execute(sql, name)
            return cursor.fetchone()

    def get_all_macros(self):
        sql = "SELECT * FROM t_directive_macro WHERE 1"
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetchall()

    def check_macro(self, macro):
        sql = """
        SELECT * FROM `directive`.`t_directive_macro` WHERE name=%s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, macro["name"])
            return cursor.fetchone()

    def save_macro(self, macro):
        print "save macro:", macro
        if macro.get("directive_macro_id"):
            self.update_macro(macro)

        exists_macro = self.check_macro(macro)
        if exists_macro:
            macro["directive_macro_id"] = exists_macro["directive_macro_id"]
            self.update_macro(macro)
        else:
            self.create_macro(macro)

    def update_macro(self, macro):
        sql = """
        UPDATE `directive`.`t_directive_macro` 
        SET 
          name=%s, 
          expression=%s,
          date_modified=now()
        WHERE 
          (`directive_macro_id`=%s)
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (macro["name"], macro["expression"], macro["directive_macro_id"]))
            self.connection.commit()

    def create_macro(self, macro):
        sql = """
        INSERT INTO `directive`.`t_directive_macro` 
        (`name`, `expression`, `date_added`) 
        VALUES 
        (%s,%s,now());
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (macro["name"], macro["expression"]))
            self.connection.commit()


class Dao:
    def __init__(self):
        self.macro = MacroRepository()
        self.schema = SchemaRepository()


dao = Dao()
