# -*- coding: UTF-8 -*-
from repository import dao
import json
import factory


def merge_dict(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


def create_macro_constant(macro_directive, suffix_directive):
    constant = macro_directive["constant"] if macro_directive.get("constant") else {}
    suffix_directive = suffix_directive["constant"] if suffix_directive.get("constant") else {}
    return merge_dict(constant, suffix_directive)


def create_macro_parameter(macro_directive, suffix_directive, name):
    param = macro_directive[name] if macro_directive.get(name) else []
    suffix_param = suffix_directive[name] if suffix_directive.get(name) else []
    if isinstance(suffix_param, list):
        return param + suffix_param
    else:
        print "suffix parameters is not list"
        return param


def create_macro_expression(name):
    full_name = name
    names = full_name.split(":")
    if len(names) > 1:
        macro_name = names[0]
        suffix_name = names[1]
    else:
        macro_name = names[0]
        suffix_name = None
    macro = dao.macro.get_macro(macro_name)
    if not macro:
        return None

    created = {
        "name": name
    }
    macro_directive = json.loads(macro["expression"])
    if suffix_name and macro_directive.get(suffix_name):
        suffix_directive = macro_directive[suffix_name]
        table_name = macro_directive["table"]
        table_schema = macro_directive["schema"]
        # 常量
        constant = create_macro_constant(macro_directive, suffix_directive)
        # 主键
        keys = create_macro_parameter(macro_directive, suffix_directive, "keys")
        # 忽略字段
        ignore = create_macro_parameter(macro_directive, suffix_directive, "ignore")
        if "update" == suffix_name.lower():
            created["expression"] = factory.create_update_expression(table_name=table_name, table_schema=table_schema,
                                                                     where_columns=keys, const_columns=constant,
                                                                     skip_columns=ignore)
        elif "insert" == suffix_name.lower():
            created["expression"] = factory.create_insert_expression(table_name=table_name, table_schema=table_schema,
                                                                     const_columns=constant,
                                                                     skip_columns=ignore + keys)
        elif "replace" == suffix_name.lower():
            created["expression"] = factory.create_replace_expression(table_name=table_name, table_schema=table_schema,
                                                                      const_columns=constant,
                                                                      skip_columns=ignore + keys)

        elif "delete" == suffix_name.lower():
            created["expression"] = factory.create_delete_expression(table_name=table_name, table_schema=table_schema,
                                                                     where_columns=keys)

        return created


def get_all_macros():
    return dao.macro.get_all_macros()


if __name__ == '__main__':
    print create_macro_expression("saveProduct:update")
    # print create_macro_directive("saveProduct:insert")
    # print create_macro_directive("saveProduct:replace")
    print create_macro_expression("saveProduct:delete")
