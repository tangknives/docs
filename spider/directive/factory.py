# -*- coding: UTF-8 -*-
import re
from repository import dao


def create_column_expression(column, const_columns):
    pattern = re.compile("varchar|text|enum")
    column_name = column["COLUMN_NAME"]
    if const_columns.get(column_name):
        return "    `%s`=%s" % (column_name, const_columns[column_name])

    if pattern.match(column["DATA_TYPE"]):
        return "    `%s`=${%s,jdbcType=VARCHAR}" % (column_name, column_name)
    else:
        return "    `%s`=${%s}" % (column_name, column_name)


def create_value_expression(column, const_columns):
    pattern = re.compile("varchar|text|enum")
    column_name = column["COLUMN_NAME"]
    if const_columns.get(column_name):
        return "    %s" % const_columns[column_name]

    if pattern.match(column["DATA_TYPE"]):
        return "    ${%s,jdbcType=VARCHAR}" % column_name
    else:
        return "    ${%s}" % column_name


def create_update_expression(table_name, table_schema, where_columns=[], const_columns={}, skip_columns=[]):
    skip_columns_set = set(skip_columns)
    where_columns_set = set(where_columns)

    update_columns = []
    where_columns = []

    for column in dao.schema.get_table_columns(table_name, table_schema):
        if column["COLUMN_NAME"] in skip_columns_set:
            continue
        if column["COLUMN_NAME"] in where_columns_set:
            where_columns.append(create_column_expression(column, const_columns))
        else:
            update_columns.append(create_column_expression(column, const_columns))

    template = """UPDATE %s.%s SET \n%s \nWHERE \n%s"""
    return template % (table_schema, table_name, ",\n".join(update_columns), ",\n    and".join(where_columns))


def create_insert_expression(table_name, table_schema, const_columns={}, skip_columns=[]):
    skip_columns_set = set(skip_columns)

    column_names = []
    column_values = []

    for column in dao.schema.get_table_columns(table_name, table_schema):
        if column["COLUMN_NAME"] in skip_columns_set:
            continue
        column_names.append("    `%s`" % column["COLUMN_NAME"])
        column_values.append(create_value_expression(column, const_columns))

    template = """INSERT INTO %s.%s (\n%s\n) VALUES (\n%s\n)"""
    return template % (table_schema, table_name, ",\n".join(column_names), ",\n".join(column_values))


def create_replace_expression(table_name, table_schema, const_columns={}, skip_columns=[]):
    skip_columns_set = set(skip_columns)
    column_names = []
    column_values = []

    for column in dao.schema.get_table_columns(table_name, table_schema):
        if column["COLUMN_NAME"] in skip_columns_set:
            continue
        column_names.append("    `%s`" % column["COLUMN_NAME"])
        column_values.append(create_value_expression(column, const_columns))

    template = """REPLACE INTO %s.%s (\n%s\n) VALUES (\n%s\n)"""
    return template % (table_schema, table_name, ",\n".join(column_names), ",\n".join(column_values))


def create_delete_expression(table_name, table_schema, where_columns):
    key_columns = []
    where_columns_set = set(where_columns)
    for column in dao.schema.get_table_columns(table_name, table_schema):
        if column["COLUMN_NAME"] in where_columns_set:
            key_columns.append(create_column_expression(column, {}))

    template = """DELETE FROM %s.%s WHERE \n%s"""
    return template % (table_schema, table_name, ",\n    and".join(key_columns))
