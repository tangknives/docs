# -*- coding: UTF-8 -*-
# 表单类，从第三方扩展的命名空间 导入
from flask_wtf import FlaskForm

# 字段类型类，字符串、布尔值、提交、密码、文本区域、选择框
from wtforms import StringField, BooleanField, SubmitField, PasswordField, TextAreaField, SelectField

# 验证器，直接从 wtforms.validators 导入
from wtforms.validators import DataRequired, Required, Length, Email, Regexp, EqualTo


# 普通用户的资料编辑表单
class DescriptionTaskForm(FlaskForm):
    url = StringField('URL', validators=[Length(1, 512)])  # 文本区域，可以多行，可以拉动
    spider_product_id = StringField('ID', validators=[Length(1, 64)])  # 文本区域，可以多行，可以拉动
    flag = StringField('FLAG', validators=[Length(1, 64)])  # 文本区域，可以多行，可以拉动
    submit = SubmitField('SUBMIT')
