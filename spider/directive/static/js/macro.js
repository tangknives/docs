Vue.component("template-show-macro", {
    template: "#template-show-macro",
    props: {
        value: {
            default: function () {
                return {}
            }
        }
    },
    data: function () {
        return {
            expression: {},
            currentSuffix: "UPDATE",
            currentName: ""
        }
    },
    computed: {
        macro: function () {
            return this.value
        }
    },
    methods: {
        open: function () {
            $(this.$el).modal()
        },
        close: function () {
            $(this.$el).modal("hide")
        },
        load: function () {
            var url = "/macro/" + this.currentName + ":" + this.currentSuffix.toLowerCase();
            axios.get(url).then(function (response) {
                this.expression = response.data.expression;
            }.bind(this))
        },
        setSuffix: function (suffix) {
            this.currentSuffix = suffix;
            this.load()
        }
    },
    watch: {
        value: function () {
            if (this.value) {
                this.currentName = this.macro.name;
                this.load();
            }
        }
    }
});

new Vue({
    el: "#app",
    data: {
        macros: [],
        currentMacro: {}
    },
    created: function () {
        this.getAllMacros()
    },
    methods: {
        getAllMacros: function () {
            var url = "/macro/all"
            axios.get(url).then(function (response) {
                this.macros = response.data
            }.bind(this))
        },
        checkMacro: function (macro) {
            if (!macro.expression) {
                this.toastError("表达式不能为空！！！")
                return false;
            }

            try {
                var expression = $.parseJSON(macro.expression);
                // this.$set(macro, "expression", JSON.stringify(expression));
            } catch (e) {
                this.toastError("表达式格式错误！！！")
                return false
            }
            return true
        },
        saveMacro: function (index) {
            var macro = this.macros[index];
            if (!this.checkMacro(macro)) {
                return;
            }
            var url = "/macro/save";
            axios.post(url, macro).then(function (response) {
                this.toastSuccess()
                this.getAllMacros()
            }.bind(this))
        },
        toastError: function (message) {
            this.$refs.showToastr.Add({
                msg: message,
                title: "Success",
                clickClose: false,
                timeout: 500,
                position: "toast-top-center",
                type: "error"
            });
        },
        toastSuccess: function () {
            this.$refs.showToastr.Add({
                msg: "Success",
                title: "操作成功",
                clickClose: false,
                timeout: 1000,
                position: "toast-top-center",
                type: "success"
            });
        },
        addMacro: function () {
            this.macros.push({
                name: null,
                expression: null
            })
        },
        showMacro: function (macro) {
            this.currentMacro = macro;
            this.$refs.showMacro.open()
        }
    },
    components: {
        'vue-toastr': window.vueToastr
    }
});
