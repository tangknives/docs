from farfetch.task import create_haozhuo_spider_task


class TaskManager:
    def __init__(self):
        self.create = True

    @staticmethod
    def create_description_task(url, flag, url_id, spider_product_id):
        return create_haozhuo_spider_task(url, flag, url_id, spider_product_id)


task_manager = TaskManager()
