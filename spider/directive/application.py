# -*- coding: UTF-8 -*-
import json

from flask import Flask, render_template, flash, redirect
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import *

from task_forms import DescriptionTaskForm
from macro import create_macro_expression, get_all_macros
from modules.tools import DatetimeEncoder
from repository import dao
from task_manager import task_manager

app = Flask(__name__)
Bootstrap(app)

nav = Nav()

# registers the "top" menubar
nav.register_element('top', Navbar(
    View('MACROS', 'macros')
))
nav.init_app(app)


@app.route('/macro/<name>')
def macro(name):
    return json.dumps(create_macro_expression(name))


@app.route('/expression/select')
def expression_select():
    return render_template('expression/select.html')


@app.route('/macro/all')
def get_all_macro():
    return json.dumps(get_all_macros(), cls=DatetimeEncoder)


@app.route('/macro/save', methods=['POST'])
def save_macro():
    dao.macro.save_macro(json.loads(request.get_data()))
    return json.dumps({"status": 0})


@app.route('/crawler/createTask', methods=['POST', "GET"])
def create_description_task():
    form = DescriptionTaskForm()
    if form.validate_on_submit():
        task_manager.create_description_task(url=form.url.data,
                                             spider_product_id=form.spider_product_id.data,
                                             url_id=0,
                                             flag=form.flag.data)
        return redirect(url_for('create_description_task'))
    else:
        form.url.data = "https://www.farfetch.cn/it/shopping/women/michel-klein-maglioncino-con-fiocco-item-12222186.aspx?storeid=9560&from=listing"
        form.spider_product_id.data = "38979"
        form.flag.data = "005"

    return render_template('crawler-task.html', form=form)


@app.route('/')
@app.route('/macros')
def macros():
    return render_template('macros.html')


app.config['SECRET_KEY'] = 'off'
if __name__ == '__main__':
    nav.init_app(app)
    app.run(debug=True, port=8504, host="0.0.0.0")
