# -*- coding: UTF-8 -*-
from bs4 import BeautifulSoup
from modules import network
# import re
import json
from offcrawler.repository import dao
from modules.scheduler import task_scheduler
from modules.repository import repositories
import time


class NetaporterProductCrawler:
    def __init__(self, task):
        self.task = task

    @staticmethod
    def get_source_id(soup):
        product_soup = soup.find(id="product")
        return product_soup["data-pid"] if product_soup else None

    @staticmethod
    def get_product_categories(soup):
        categories_soup = soup.find("meta", attrs={"itemprop": "category"})
        if categories_soup and categories_soup.get("content"):
            return [item.strip() for item in categories_soup["content"].split("/")]
        return []

    @staticmethod
    def get_product_sizes(soup):
        sizing_soup = soup.find(class_="sizing-container")
        if not sizing_soup:
            return None

        dropdown_soup = sizing_soup.find("select-dropdown")
        if dropdown_soup and dropdown_soup.get("options"):
            return json.loads(dropdown_soup["options"])

    def create_product(self, soup):
        print(self.get_product_categories(soup))
        print(json.dumps(self.get_product_sizes(soup)))
        print(self.get_source_id(soup))
        dao.buyer.add_picture_tags(self.get_source_id(soup), 2, self.get_product_categories(soup))

    def crawl(self, url):
        print "begin url:", url

        html = network.download(url)
        soup = BeautifulSoup(html, "html.parser")
        self.create_product(soup)

        print "end url:", url

    def start(self):
        url = "https://www.net-a-porter.com/" + self.task["url"]
        self.crawl(url)


if __name__ == "__main__":
    repositories.connect()
    task_scheduler.connect()
    # task = {
    #     "url": "/cn/zh/product/992006/erdem/wanda-----------------"
    # }
    # dao.task.create_product_tasks()
    while True:
        task = dao.task.get_picture_task()
        if task:
            NetaporterProductCrawler(task).start()
        else:
            time.sleep(15)
