# -*- coding: UTF-8 -*-
from bs4 import BeautifulSoup
from modules import network
import re
import json
from offcrawler.repository import dao
from modules.repository import repositories


class NetaporterCatalogCrawler:
    def __init__(self, task):
        self.task = task

    @staticmethod
    def get_source_id(url):
        pattern = re.compile("/cn/zh/product/(\d+)/.*")
        match = pattern.match(url)
        return match.group(1)

    @staticmethod
    def get_big_image_urls(source_id):
        # pattern = re.compile("//cache.net-a-porter.com/images/products/(\d+/\d+)_in_sl.jpg")
        return [
            "//cache.net-a-porter.com/images/products/%s/%s_in_xl.jpg" % (source_id, source_id),
            "//cache.net-a-porter.com/images/products/%s/%s_ou_xl.jpg" % (source_id, source_id)
        ]

    @staticmethod
    def get_big_cover_url(source_id):
        return "//cache.net-a-porter.com/images/products/%s/%s_ou_xl.jpg" % (source_id, source_id)

    def create_catalogs(self, soup):
        products_soup = soup.find("ul", class_="products")
        if not products_soup:
            return

        for item in products_soup.find_all("li"):
            image_soup = item.find("div", class_="product-image")
            designer_soup = item.find("span", class_="designer")
            price_soup = item.find("span", class_="price")

            url = image_soup.a["href"]
            image_outfit = image_soup.img["data-image-outfit"]
            image_product = image_soup.img["data-image-product"]
            name = image_soup.img["alt"]
            designer_name = designer_soup.string.strip()
            sale_price = price_soup.string.strip()
            source_id = self.get_source_id(url)

            product = {
                "url": url,
                "thumbnail": image_outfit,
                "product_name": name,
                "source_type": 2,
                "source_id": self.get_source_id(url),
                "catalog_status": 0,
                "language_id": self.task["language_id"],
                "catalog_context": json.dumps({
                    "designer_name": designer_name,
                    "sale_price": sale_price,
                    "stock": "NA",
                    "image_outfit": image_outfit,
                    "image_product": image_product,
                    "image_urls": self.get_big_image_urls(source_id)
                })
            }
            dao.catalog.save_catalog(product)

            picture = {
                "picture_name": name,
                "tags": json.dumps([designer_name]),
                "cover": self.get_big_cover_url(source_id),
                "description": None,
                "url": url,
                "source_type": 2,
                "source_id": self.get_source_id(url),
                "image_urls": json.dumps(self.get_big_image_urls(source_id))
            }
            dao.buyer.save_picture(picture)

    def crawl(self, page_url, task):
        print "begin url:", page_url
        html = network.download(page_url)
        soup = BeautifulSoup(html, "html.parser")
        self.create_catalogs(soup)
        print "end url:", page_url
        return soup

    @staticmethod
    def get_total_page(soup):
        pagination = soup.find(class_="pagination-links")
        return int(pagination["data-lastpage"]) if pagination else 0

    def start(self):
        url = self.task["url"]
        soup = self.crawl(url, self.task)
        total_page = self.get_total_page(soup)
        print "total page(%d):" % total_page, url

        pattern = re.compile(r"(pn=\d+)")
        for i in range(2, total_page):
            page_url = pattern.sub(r"pn=%d" % i, url)  # url.replace("pn=%d", "pn=%s" % i)
            self.crawl(page_url, self.task)

            # dao.task.update_task_status(task)


if __name__ == "__main__":
    repositories.connect()
    task = {
        "url": "https://www.net-a-porter.com/cn/zh/d/Shop/Clothing/All?cm_sp=topnav-_-clothing-_-topbar&pn=1&npp=60&image_view=product&dscroll=0",
        "language_id": 1
    }
    NetaporterCatalogCrawler(task).start()
