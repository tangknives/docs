# -*- coding: UTF-8 -*-
from .bucket import OssClient, imagesBucket, oldBucket

oss_client = OssClient(imagesBucket)
old_oss_client = OssClient(oldBucket)
