# -*- coding: UTF-8 -*-
import pymysql
import time
import json
import smtplib
from email.mime.text import MIMEText
from email.header import Header


def send_mail(to_user, from_user, receivers):
    to_name = to_user['name'] or to_user['email']
    from_name = from_user['name'] or from_user['email']
    sender = 'offservice@fashionfinger.com'
    subject = '[收件箱]OFF商家注册申请'
    smtpserver = 'smtp.fashionfinger.com'
    username = 'service@off-service.com'
    password = 'off_Service1011'

    text = '''
    <html>
        <h4>Dear %s，</h4>
        <p>
            <span>您已通过OFF的开店预约申请，通过该链接地址 http://www.off-online.com/businessRegister.html 完成商家注册，OFF SERVICE 已经在APP STORE上线，正在内测阶段，请等待我们新的邮件通知，届时可直接通过注册账号登陆使用OFF SERVICE，线上折扣库存功能与图库功能及个人商城功能都将开放使用，如果您有供货需求和现货库存，是可以申请成为OFF供货商户的。如您有任何问题可随时联系我们，内测阶段，我们将会以微信的形式更新提供商品库存、售价及图片，可以添加我们的客服微信号：OFF-SERVICE</span><br/>
        </p>
        <p>
        敬祝 商祺！<br/>
        Best Regards,<br/>
        OFF SERVICE<br/>
        </p>
    </html>
    '''
    msg = MIMEText(text % 'adam', 'html', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')
    msg['From'] = from_name
    msg['To'] = to_name

    smtp = smtplib.SMTP()
    smtp.connect(smtpserver)
    smtp.login(username, password)
    senderrs = smtp.sendmail(sender, receivers, msg.as_string())
    smtp.quit()

    print(senderrs)


if __name__ == "__main__":
    # Transfer().transfer_designers()
    # Transfer().create_source_seasons()
    # Transfer().transfer_products(8)
    to_user = {
        "name": "yuanjie"
    }
    from_user = {
        "name": "off-service"
    }

    receivers = ["252675627@qq.com", "yuanjie@fashionfinger.com"]
    send_mail(to_user, from_user, receivers)
