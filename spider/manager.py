import time
from sys import argv
from modules.scheduler import task_scheduler
from modules.repository import repositories
from mozia.crawler.downloader import start_images_downloader, start_download_oss_covers
from mozia.crawler.catalog import crawl_intramirror_catalogs, crawl_farfetch_catalogs
from mozia.crawler.tools.save_cover_to_oss import start_cover_uploader
from mozia.crawler.monitor import start_intramirror_monitor, start_farfetch_monitor

funcs = {
    "start_intramirror_monitor": 1,
    "start_farfetch_monitor": 1,
    "start_images_downloader": 1,
    "crawl_intramirror_catalogs": 1,
    "crawl_farfetch_catalogs": 1,
    "start_download_oss_covers": 1,
    "start_cover_uploader": 1
}


def usage(argv):
    print "Usage:"
    for name in funcs:
        print "     python ", argv[0], name


if __name__ == "__main__":

    # offcrawler.application.app
    print("args:", argv)
    func_name = argv[1] if len(argv) > 1 else None
    if not func_name:
        usage(argv)
        exit(0)

    if not funcs.get(func_name):
        print "Invalid function :", func_name
        usage(argv)
        exit(0)

    task_scheduler.connect()
    repositories.connect()
    # task_scheduler.connect(nodes=[{"host": "10.26.235.6", "port": "6379"}])
    # repositories.connect(host="10.26.235.6", port=3066)

    print "begin", time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    eval(func_name)(*(argv[2:]))
    print "end", time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
