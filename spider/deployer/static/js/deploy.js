Vue.component("tail-logs", {
    template: "#template-tail-logs",
    props: {
        url: "/webadm/logs"
    },
    data: function () {
        return {
            placeholder: "正在获取日志，不要关闭窗口...",
            logs: null,
            timer: null
        }
    },
    methods: {
        get_logs: function () {
            axios.get(this.url).then(function (response) {
                this.logs = response.data
            }.bind(this))
        },
        start: function () {
            if (this.timer) {
                this.stop();
            }

            this.timer = setInterval(function () {
                this.get_logs()
            }.bind(this), 5000);
        },
        stop: function () {
            clearInterval(this.timer)
            this.timer = null
        },
        close: function () {
            $(this.$el).modal("hide");
            this.stop()
        },
        open: function () {
            this.logs = null;
            this.get_logs();
            this.start();
            $(this.$el).modal()
        }
    }
});

new Vue({
    el: '#app',
    data: function () {
        return {
            weboffStatus: null,
            webffoStatus: null,
            webadmStatus: null,
            officialStatus: null,
            fingerStatus: null,
            weboffPackage: null,
            webffoPackage: null,
            webadmPackage: null,
            officialPackage: null,
            fingerPackage: null,
            weboffProgress: 0,
            webffoProgress: 0,
            webadmProgress: 0,
            officialProgress: 0,
            fingerProgress: 0,
            deployMessage: null,
            tailLogsUrl: null
        }
    },
    mounted: function () {
        catalog = {
            "source_type": 1,
            "catalog_id": 7909,
            "language_id": 1,
            "source_id": "12587559",
            "url": "/cn/shopping/women/dolce-gabbana-leopard-trim-cropped-trousers-item-12587559.aspx?storeid=9306&from=listing",
            "thumbnail": "https://cdn-images.farfetch-contents.com/12/58/75/59/12587559_12078291_255.jpg",
            "product_name": "leopard trim cropped trousers",
            "catalog_status": 0
        };
        axios.post("http://www.fashionfinger.com:9604/proxy/product", catalog, {})
            .then(function (respponse) {
                console.log(respponse)
            })
    },
    computed: {
        webffoTitle: function () {
            return this.getDeployStatus(this.webffoStatus) + "(" + this.webffoProgress + ")"
        },
        weboffTitle: function () {
            return this.getDeployStatus(this.weboffStatus) + "(" + this.weboffProgress + ")"
        },
        webadmTitle: function () {
            return this.getDeployStatus(this.webadmStatus) + "(" + this.webadmProgress + ")"
        },
        officialTitle: function () {
            return this.getDeployStatus(this.officialStatus) + "(" + this.officialProgress + ")"
        },
        fingerTitle: function () {
            return this.getDeployStatus(this.fingerStatus) + "(" + this.fingerProgress + ")"
        }
    },
    methods: {
        getDeployStatus: function (readyStatus) {
            if (0 === readyStatus) {
                return "上传文件中"
            } else if (1 === readyStatus) {
                return "已就绪"
            } else if (2 === readyStatus) {
                return "部署中"
            } else if (3 === readyStatus) {
                return "已部署"
            } else {
                return "未上传文件"
            }
        },
        uploadFile: function (e, onUploadProgress) {
            e.preventDefault();
            var file = e.target.files[0];

            if (!file) {
                return
            }

            var formData = new FormData();
            formData.append('file', file);
            var config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: onUploadProgress
            };

            return axios.post('/file/upload', formData, config)
        },
        uploadOfficial: function (e) {
            if (!e.target.files[0]) {
                return
            }

            this.officialStatus = 0;
            this.officialPackage = null;
            this.officialProgress = 0;
            this.uploadFile(e, function (progressEvent) {
                this.officialProgress = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            }.bind(this)).then(function (response) {
                this.officialPackage = response.data;
                this.officialStatus = 1
            }.bind(this))
        },
        uploadFinger: function (e) {
            if (!e.target.files[0]) {
                return
            }

            this.fingerStatus = 0;
            this.fingerPackage = null;
            this.fingerProgress = 0;
            this.uploadFile(e, function (progressEvent) {
                this.fingerProgress = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            }.bind(this)).then(function (response) {
                this.fingerPackage = response.data;
                this.fingerStatus = 1
            }.bind(this))
        },
        uploadWeboff: function (e) {
            if (!e.target.files[0]) {
                return
            }

            this.weboffStatus = 0;
            this.weboffPackage = null;
            this.weboffProgress = 0;
            this.uploadFile(e, function (progressEvent) {
                this.weboffProgress = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            }.bind(this)).then(function (response) {
                this.weboffPackage = response.data;
                this.weboffStatus = 1
            }.bind(this))
        },
        uploadWebffo: function (e) {
            if (!e.target.files[0]) {
                return
            }

            this.webffoStatus = 0;
            this.webffoProgress = 0;
            this.webffoPackage = null;
            this.uploadFile(e, function (progressEvent) {
                this.webffoProgress = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            }.bind(this)).then(function (response) {
                this.webffoPackage = response.data;
                this.webffoStatus = 1;
            }.bind(this))
        },
        uploadWebadm: function (e) {
            if (!e.target.files[0]) {
                return
            }

            this.webadmStatus = 0;
            this.webadmProgress = 0;
            this.webadmPackage = null;
            this.uploadFile(e, function (progressEvent) {
                this.webadmProgress = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            }.bind(this)).then(function (response) {
                this.webadmPackage = response.data;
                this.webadmStatus = 1;
            }.bind(this))
        },
        deployFinger: function () {
            if (!this.fingerPackage || this.fingerStatus !== 1) {
                return
            }

            this.fingerStatus = 2;
            axios.post('/finger/deploy', {
                source: this.fingerPackage
            }).then(function (response) {
                this.fingerStatus = 3;
                this.deployMessage = response.data;
            }.bind(this))
        },
        deployOfficial: function () {
            if (!this.officialPackage || this.officialStatus !== 1) {
                return
            }

            this.officialStatus = 2;
            axios.post('/official/deploy', {
                source: this.officialPackage
            }).then(function (response) {
                this.officialStatus = 3;
                this.deployMessage = response.data;
            }.bind(this))
        },
        deployWeboff: function () {
            if (!this.weboffPackage || this.weboffStatus !== 1) {
                return
            }

            this.weboffStatus = 2;

            axios.post('/weboff/deploy', {
                source: this.weboffPackage
            }).then(function (response) {
                this.weboffStatus = 3;
                this.deployMessage = response.data;
            }.bind(this))
        },
        deployWebffo: function () {
            if (!this.webffoPackage || this.webffoStatus !== 1) {
                return
            }

            this.webffoStatus = 2;
            axios.post('/webffo/deploy', {
                source: this.webffoPackage
            }).then(function (response) {
                this.webffoStatus = 3;
                this.deployMessage = response.data;
            }.bind(this))
        },
        deployWebadm: function () {
            if (!this.webadmPackage || this.webadmStatus !== 1) {
                return
            }

            this.webadmStatus = 2;
            axios.post('/webadm/deploy', {
                source: this.webadmPackage
            }).then(function (response) {
                this.webadmStatus = 3;
                this.deployMessage = response.data;
            }.bind(this))
        },
        restartWeboff: function () {
            axios.get('/weboff/restart').then(function (response) {
                this.deployMessage = response.data;
            }.bind(this))
        },
        restartWebffo: function () {
            axios.get('/webffo/restart').then(function (response) {
                this.deployMessage = response.data;
            }.bind(this))
        },
        restartWebadm: function () {
            axios.get('/webadm/restart').then(function (response) {
                this.deployMessage = response.data;
            }.bind(this))
        },
        tailWebffoLogs: function () {
            this.tailLogsUrl = "/webffo/logs";
            this.$refs.tailLogs.open()
        },
        tailWeboffLogs: function () {
            this.tailLogsUrl = "/weboff/logs";
            this.$refs.tailLogs.open()
        },
        tailWebadmLogs: function () {
            this.tailLogsUrl = "/webadm/logs";
            this.$refs.tailLogs.open()
        }
    }
});