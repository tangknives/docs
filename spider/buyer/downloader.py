# -*- coding: UTF-8 -*-
import os
import json
from modules.repository import repositories
from buyer.repository import dao
from modules import network
from modules.oss import oss_client


def download_image(picture_id, url):
    oss_url = "pictures/%s/%s" % (picture_id, os.path.basename(url))
    full_oss_url = "https://images-media.oss-cn-shanghai.aliyuncs.com/%s" % oss_url
    if oss_client.exists(oss_url):
        print("image exists:", oss_url)
        return full_oss_url
    content = network.download(url)
    oss_client.put_object(oss_url, content)
    return full_oss_url


def download_images(picture_id, image_urls):
    return [
        download_image(picture_id, url) for (index, url) in enumerate(image_urls)
    ]


def download_picture_images():
    for picture in dao.buyer.get_pictures():
        picture_id = picture["picture_id"]
        image_urls = json.loads(picture["image_urls"])
        oss_urls = download_images(picture_id, image_urls)
        cover_url = download_image(picture_id, picture["cover"])
        dao.buyer.save_picture_images(picture_id, cover_url, oss_urls)


if __name__ == "__main__":
    repositories.connect()
    download_picture_images()
