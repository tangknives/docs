# -*- coding: UTF-8 -*-
from modules.repository import Repository
from modules.scheduler import task_scheduler
import json


class PlatformRepository(Repository):
    def __init__(self):
        Repository.__init__(self, db="fashion_product")

    def get_product_sku_images(self, product_id):
        sql = """
        SELECT a.* FROM fashion_product.t_product_sku a 
        WHERE a.product_id=%s 
        AND a.sku_images IS NOT NULL 
        AND a.sku_images <> 'null' 
        AND trim(a.sku_images) <> '' 
        """

        with self.connection.cursor() as cursor:
            cursor.execute(sql, product_id)
            sku = cursor.fetchone()
        try:
            return json.loads(sku["sku_images"] or '[]') if sku else None
        except:
            return None

    def save_product_images(self, product_id, cover, image_urls):
        sql = """
        UPDATE fashion_product.t_product a,
         fashion_product.t_product_sku b
        SET a.cover = %s,
         b.sku_cover = %s,
         b.sku_images = %s,
         b.remark = 'crawler'
        WHERE
            a.product_id = b.product_id
        AND a.product_id = %s
        """
        print("set product cover:", cover)
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (cover, cover, json.dumps(image_urls), product_id))
        self.connection.commit()


class BuyerRepository(Repository):
    def __init__(self):
        Repository.__init__(self, db="fashion_buyer")

    def create_picture(self, picture):
        sql = """
       INSERT INTO `fashion_buyer`.`t_picture` 
       (`picture_id`,`name`, `cover`, `cover_type`, `description`, `type`, `date`, `tags`, `image_urls`, `date_added`) 
       VALUES 
       (%(picture_id)s, %(picture_name)s, %(cover)s, '0', %(description)s , '3',now(), %(tags)s, %(image_urls)s, now());
       """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, picture)
            lastrowid = cursor.lastrowid
        self.connection.commit()
        return lastrowid

    def save_picture_images(self, picture_id, cover_url, image_urls):
        sql = """
        UPDATE fashion_buyer.t_picture SET cover=%s, image_urls=%s WHERE picture_id=%s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (cover_url, json.dumps(image_urls),picture_id))
        self.connection.commit()

    def check_picture_source(self, picture):
        sql = """
        SELECT * FROM fashion_buyer.t_picture_source WHERE source_id=%s AND source_type=%s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (picture["source_id"], picture["source_type"]))
            return cursor.fetchone()

    def create_picture_source(self, picture):
        sql = """
        INSERT INTO `fashion_buyer`.`t_picture_source` 
        (`source_id`, `source_type`, `url`, `date_added`) 
        VALUES 
        (%(source_id)s, %(source_type)s, %(url)s, now());
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, picture)
            lastrowid = cursor.lastrowid
        self.connection.commit()
        return lastrowid

    def update_picture_source(self, picture):
        sql = """
        UPDATE `fashion_buyer`.`t_picture_source` 
        SET 
        `source_id`=%(source_id)s, 
        `source_type`=%(source_type)s, 
        `url`=%(url)s,  
        `date_modified`=now() WHERE (`picture_id`=%(picture_id)s);
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, picture)
        self.connection.commit()

    def update_picture(self, picture):
        print "update picture:", picture
        sql = """
        UPDATE `fashion_buyer`.`t_picture` SET 
        `name`=%(picture_name)s, 
        `cover`=%(cover)s, 
        `cover_type`='0', 
        `description`=%(description)s, 
        `type`='3', 
        `image_urls`=%(image_urls)s, 
        `date_modified`=now() 
        WHERE (`picture_id`=%(picture_id)s);
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, picture)
        self.connection.commit()

    def check_picture(self, picture):
        sql = """
        SELECT * FROM fashion_buyer.t_picture WHERE `picture_id` = %s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, picture["picture_id"])
            return cursor.fetchone()

    def save_picture(self, picture):
        # FIXME: 应该创建picture_source 再创建picture,这样可以支持name相同多个图片来源
        picture_source = self.check_picture_source(picture)
        if picture_source:
            picture["picture_id"] = picture_source["picture_id"]
            self.update_picture_source(picture)
        else:
            picture["picture_id"] = self.create_picture_source(picture)

        if self.check_picture(picture):
            self.update_picture(picture)
        else:
            self.create_picture(picture)

    def get_picture_by_source(self, source_id, source_type):
        sql = """
        SELECT
            b.*
        FROM
            fashion_buyer.t_picture b,
            fashion_buyer.t_picture_source a
        WHERE
            a.picture_id = b.picture_id
        AND a.source_id = %s
        AND a.source_type = %s
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (source_id, source_type))
            return cursor.fetchone()

    def add_picture_tags(self, source_id, source_type, tags):
        if not tags:
            return

        picture = self.get_picture_by_source(source_id, source_type)
        if not picture:
            print("picture source no found:", source_id, source_type)
            return

        exists_tags = set(json.loads(picture["tags"]) or [])
        new_tags = list(set(tags) | exists_tags)

        sql = """
        UPDATE fashion_buyer.t_picture SET tags=%s WHERE picture_id=%s
        """
        print("add tags for picture:", picture["picture_id"], new_tags)
        with self.connection.cursor() as cursor:
            cursor.execute(sql, (json.dumps(new_tags).decode("unicode-escape"), picture["picture_id"]))
        self.connection.commit()

    def get_pictures(self):
        sql = """
        SELECT * FROM fashion_buyer.t_picture where picture_id>1536
        """
        with self.connection.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetchall()


class TaskRepository(Repository):
    def __init__(self):
        Repository.__init__(self, db="fashion_buyer")


class Dao:
    def __init__(self):
        self.catalog = None
        self.create_repositories()

    def create_repositories(self):
        self.task = TaskRepository()
        self.buyer = BuyerRepository()
        self.platform = PlatformRepository()

    def reset(self):
        self.create_repositories()


dao = Dao()
