import requests
from bs4 import BeautifulSoup
from dao import connection
import re


class Configure:
    data = {}

    @staticmethod
    def get_configure():
        sql = """
        SELECT * FROM t_configure
        """
        with connection.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetchall()

    def __init__(self):
        for pair in self.get_configure():
            name = pair["name"]
            setattr(self, name, pair["value"])


configure = Configure()


def get_items_number():
    url = "http://www.greenbox.cloud/colorscontainer/colors"
    page = requests.get(url, headers={
        "Cookie": configure.cookie
    })
    soup = BeautifulSoup(page.content, "html.parser")
    """
    <span _ngcontent-oqx-9="">
        Items: 2124
    </span>
    """
    soup = soup.find("span", text=re.compile("Items"))

    print soup


class CatalogSpider:
    def save(self, url, context):
        sql = """
        REPLACE INTO `greenbox`.`t_spider_page` (`url`, `context`, `date_added`) VALUES (%s, %s, now())
        """
        with connection.cursor() as cursor:
            cursor.execute(sql, (url, context))
            connection.commit()

    def fetch(self, i):
        url = configure.url % i
        print("begin url:" + url)
        page = requests.get(url, headers={
            "Cookie": configure.cookie,
            "Referer": "http://www.greenbox.cloud/colorscontainer/colors"
        })
        self.save(url, page.content)
        print("end url:" + url)

    def start(self):
        for i in range(1, 55):
            self.fetch(i)


if __name__ == "__main__":
    CatalogSpider().start()
