from rediscluster import StrictRedisCluster


class RedisTester():
    nodes = [{"host": "172.16.8.147", "port": "6379"}]

    def __init__(self):
        self.redis = StrictRedisCluster(startup_nodes=self.nodes, decode_responses=True)
        self.redis.append("name", "ddd")
        print self.redis.get("name")


RedisTester()
