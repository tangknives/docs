﻿# ims库存系统
## 一、目录结构
    directive
    |__ dependencies                                      依赖包
    |__ modules                                           公共模块
           |__ booter                                     web启动模块
           |__ datatables                                 指令实现模块(项目核心模块)
           |__ utils                                      公共类
           |__ gmybatis                                   mybats封装(已停止使用)
           |__ message                                    消息处理模块
           |__ mq                                         mq封装
           |__ shiro                                      shiro封装
           |__ sms                                        短信发送
           |__ validator                                  参数验证注解
           |__ webjars                                    web包
           |__ words                                      分词处理
    |__ mock                                        mock数据
    |__ modules                                     模块
    |      |__ administration
    |      |__ product
    |      |__ index.js                                   
    |__ portal                                      布局、路由等配置
    |__ supports                                       
    |__ utils
    |__ main.js                                     程序入口

## 二、组件
### 1. 定义
```javascript
import Vue from 'vue'
import Component from 'vue-class-component'

@Component
export default class Sample extends Vue {
    render () {
        return <div> Sample component </div>
    }
}
```
### 2. 使用
```javascript
import Vue from 'vue'
import Sample from 'Sample.js'

new Vue({
    el: '#app',
    render: h => h(Sample)
})
```
## 三、模块
为了让代码结构清晰，ims通过业务功能划分成多个模块，具体见src/modules/*。按该方式拆分后，每个业务模块维护自身的模块定义(module.js)、国际化文件(i18n.js)等。通过**webpack**提供的require.context方法自动收集模块的(路由、权限、国际化)等资源。

require.context() 是什么？ 是webpack提供的通过正则匹配动态引入相应的文件模块。
require.context 有三个参数：

- directory：说明需要检索的目录
- useSubdirectories：是否检索子目录
- regExp: 匹配文件的正则表达式

### 1. 模块定义(module.js)
 - 节点
     - menuSet 渲染为导航栏的下拉菜单，
        - 属性
            >* name：模块资源名称
            >* children：menuItem集合

        - 约束
            >* name：只能为根节点
            
     - menuItem 菜单元素, 如无父节点，渲染为导航栏的菜单元素，如有父节点则渲染为导航栏下拉选项      
         - 属性
            >* name：模块资源名称(路由名称)
            >* path: 路由地址 
            >* component: 路由所指向的vue组件
            >* children：menuItem集合
            >* operations: panel、routeTrigger、function集合
            
     - panel 选项卡资源
        - 属性
            >* name：模块资源名称(路由名称)
            >* operations: panel、routeTrigger、function集合
        
        - 说明
          >* 选项卡资源定义，用于与权限收集，页面具体渲染需要手动实现
    
     - routeTrigger 路由触发器
        - 属性
            >* name: 模块资源名称(路由名称)
            >* path: 路由地址 
            >* component: 路由所指向的vue组件
            >* operations: panel、routeTrigger、function集合
        
        - 说明
          >* 路由与权限资源定义，用于注册路由资源与权限收集
     
     - function 操作资源
        - 属性
            >* name: 模块资源名称(路由名称)

        - 说明
          >* 权限资源定义，用于与权限收集
          
 - 例子
 ```javascript
 export default {
     type: 'menuItem',
     name: 'designers',
     path: '/designers',
     component: resolve => require(['./xxx'], component => { resolve(component.default) }),
     operations: [
        {
          type: 'routeTrigger',
          name: 'createDesigner',
          path: '/designers/create',
          meta: {
            operation: 'create'
          },
          component: resolve => require(['./xxx'], component => { resolve(component.default) })
        },
        {
          type: 'routeTrigger',
          name: 'updateDesigner',
          path: '/designers/update/:id'
          component: resolve => require(['./xxx'], component => { resolve(component.default) })
        }
     ]
 }
 ```
 
 - 注意 
 >* 模块定义文件必须命名为module.js
 >* 所有模块资源会根据匹配到的路由自动加载资源，无需手动引入

### 2. 国际化资源文件(i18n.js)
vue-i18n资源定义见[文档](http://kazupon.github.io/vue-i18n/old/)

## 四、UI框架（[Element UI](https://github.com/ElemeFE/element)）
**Element UI** 已在main.js中全局注册，具体使用见,[文档](http://element.eleme.io)

## 五、AJAX（[axios](https://github.com/axios/axios)）
**axios** 相关设置在src/utils/axios/index.js文件文件中

### 1. 组件内使用

>* get json
    ```javascript
    this.$axios.get(url, params).then(response => console.log(response))
    ```
    
>* post json
    ```javascript
    this.$axios.post(url, params).then(response => console.log(response))
    ```
    
>* post form
    ```javascript
    this.$axios.postForm(url, params).then(response => console.log(response))
    ```

### 2. 模块中引用

```javascript
import axios from '@/utils/axios'

axios.get(url, params).then(response => console.log(response))
```